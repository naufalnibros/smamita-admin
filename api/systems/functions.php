<?php

function insertSiswa($data){
    $db = new \Cahkampung\Landadb(Db());
    $inputsiswa['nama'] = $data['nama'];
    $inputsiswa['username'] = $data['nipd'];
    $inputsiswa['password'] = sha1($data['nipd']);
    $inputsiswa['type'] = 'siswa';
    $inputsiswa['maste_role_id'] = 4;
    $inputsiswa['telepon'] = $data['telepon'];
    $inputsiswa['master_role_id'] = 4;
    $proses_input_siswa = $db->insert("master_user",$inputsiswa);
    $inputwali['nama'] = "Wali Murid : ". $data['nama'];
    $inputwali['username'] = $data['telepon_ortu'];
    $inputwali['password'] = sha1($data['telepon_ortu']);
    $inputwali['type'] = 'walimurid';
    $inputwali['maste_role_id'] = 3;
    $inputwali['telepon'] = $data['telepon'];
    $inputwali['master_role_id'] = 3;
    $proses_input_wali = $db->insert("master_user",$inputwali);
    $data['master_user_id_siswa'] = $proses_input_siswa->id;
    $data['master_user_id_walimurid'] = $proses_input_wali->id;
    $proses_input_data_siswa = $db->insert("master_siswa",$data);
   return $proses_input_data_siswa;
}
/**
 * format hari
 *
 * @param String from date ("D");
 * @return String
 */
function format_hari($hari) {
    switch ($hari) {
        case 'Sun':
            $hari_ini = "Minggu";
            break;
        case 'Mon':
            $hari_ini = "Senin";
            break;
        case 'Tue':
            $hari_ini = "Selasa";
            break;
        case 'Wed':
            $hari_ini = "Rabu";
            break;
        case 'Thu':
            $hari_ini = "Kamis";
            break;
        case 'Fri':
            $hari_ini = "Jumat";
            break;
        case 'Sat':
            $hari_ini = "Sabtu";
            break;
        default:
            $hari_ini = "Tidak di ketahui";
            break;
    }
    return $hari_ini;
}
function tgl_indo($datetime){
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $split = explode('-', date("Y-m-d",$datetime));
    return format_hari(date("D", $datetime)) . ', ' . $split[2]  . ' ' . $bulan[(int) $split[1]] . ' ' . $split[0];
}
function validate($data, $validasi, $custom = [])
{
    if (!empty($custom)) {
        $validasiData = array_merge($validasi, $custom);
    } else {
        $validasiData = $validasi;
    }
    $validate = GUMP::is_valid($data, $validasiData);
    if ($validate === true) {
        return true;
    } else {
        return $validate;
    }
}
function get_images($html)
{
    preg_match_all('@src="([^"]+)"@' , $html, $match);
    $src = array_pop($match);
    return $src;
}
function get_iframe($html)
{
    $result = preg_match_all('/<img.*?src\s*=.*?>/', $html, $matches, PREG_SET_ORDER);
    for ($i=0; $i < count($matches); $i++) {
        $img = $matches[$i][0];
        $a = get_youtube($img);
        $iframe = '<iframe width="'.getenv('youtube_width').'" height="'.getenv('youtube_height').'" src="'.$a[0].'">
        </iframe>';
        $yutub = str_replace($img, $iframe , $img);
        $konten = str_replace($img, $yutub, $html);
        $html = $konten;
    }
   return $html;
}
function get_youtube($img)
{
    preg_match_all( '@ta-insert-video="([^"]+)"@' , $img, $match);
    $src = array_pop($match);
    return $src;
}
function base64_to_jpeg($base64_string) {
    // open the output file for writing
    $output_file = null;
    $ifp = fopen( $output_file, 'wb' );
    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );
    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
    // clean up the file resource
    fclose( $ifp );
    return $output_file;
}
function imgUrl($custom = NULL){
  $url = getenv('SITE_IMG');
  if (isset($custom)) {
    return $url.$custom;
  } else {
    return $url;
  }
}