<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'jenis' => 'required',

    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get('/appprestasi/siswa', function ($request, $response) {
    $db = $this->db;


    try {
        $siswa = $db->select("master_siswa.*")
            ->from("master_siswa")
            ->findAll();
        return successResponse($response, $siswa);
    } catch (Exception $e) {
        return unprocessResponse($response, ['Tidak ada data siswa']);
    }
});

/**
 * Get list user roles
 */
$app->get('/appprestasi/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("master_prestasi.*, master_siswa.nama as namasiswa")
            ->from("master_prestasi")
            ->leftJoin("master_siswa","master_siswa.id = master_prestasi.master_siswa_id");


    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'keterangan') {
              $db->where('master_prestasi.keterangan', 'LIKE', $val);
            }else if ($key == 'siswa'){
                $db->andWhere('master_prestasi.namasiswa', 'LIKE', $val);
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();

    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save roles
 */
$app->post('/appprestasi/save', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {

            if (isset($request->getUploadedFiles()["attachment"])){
                // Mengambil File dan Mengambil nama
                $attachment["body"] = $request->getUploadedFiles()["attachment"];
                $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                // Menyimpan File ke directory tertentu
                $attachment["body"]->moveTo("storage/attachment_prestasi/{$attachment['name_file']}");
                $data["attachment"] = $attachment["name_file"];
            }


            $model = $db->insert("master_prestasi",$data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appprestasi/delete/{id}', function ($request, $response) {
    $db = $this->db;
    $id = $request->getAttribute('id');
    try {

        $file = $db->select("*")
            ->from("master_prestasi")
            ->where("id","=",$id)
            ->find();
        $filename= $file->attachment;
        if (isset($filename)){
            $path = 'storage/attachment_prestasi/'.$filename;
            unlink($path);
        }

        $delete = $db->delete('master_prestasi', array('id' => $id));
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }
});


$app->post('/appprestasi/update', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;



    $validasi = validasi($data);



    if ($validasi === true) {
        try {

            if (isset($request->getUploadedFiles()["attachment"])){

                $file = $db->select("*")
                    ->from("master_prestasi")
                    ->where("id","=",$data["id"])
                    ->find();
                $filename= $file->attachment;

                if (isset($filename)){
                    $path = 'storage/attachment_prestasi/'.$filename;
                    unlink($path);
                    $attachment["body"] = $request->getUploadedFiles()["attachment"];
                    $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                    // Menyimpan File ke directory tertentu
                    $attachment["body"]->moveTo("storage/attachment_prestasi/{$attachment['name_file']}");
                    $data["attachment"] = $attachment["name_file"];
                }else{
                    $attachment["body"] = $request->getUploadedFiles()["attachment"];
                    $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                    // Menyimpan File ke directory tertentu
                    $attachment["body"]->moveTo("storage/attachment_prestasi/{$attachment['name_file']}");
                    $data["attachment"] = $attachment["name_file"];
                }

                $model = $db->update("master_prestasi", $data, array('id' => $data['id']));
                return successResponse($response, $model);
            }else{

                $model = $db->update("master_prestasi", $data, array('id' => $data['id']));
                return successResponse($response, $model);
            }



        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});
