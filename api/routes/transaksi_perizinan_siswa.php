<?php

use Slim\Http\Request;
use Slim\Http\Response;


function validasi($data, $custom = array()) {
    $validasi = array(
        'jam_keluar' => 'required',
        'jam_kembali' => 'required',
        'm_user_id' => 'required',

    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get('/transaksi_perizinan_siswa/listPerizinan', function (Request $request, Response $response) {
    $db = $this->db;
    $siswa = $db->find('select id  from master_siswa where master_user_id_siswa = "' . $_SESSION['user']['id'] . '"');

    $data = $db->select('transaksi_perizinan.*,master_user.nama')
        ->from('transaksi_perizinan')
        ->leftJoin("master_user","master_user.id = transaksi_perizinan.master_user_id ")
        ->where('master_siswa_id','=',$siswa->id)
        ->findAll();
    return successResponse($response, $data);

});

$app->post('/transaksi_perizinan_siswa/save', function ($request, $response) {
    $data = $request->getParams();



    $db = $this->db;

    $awal = strtotime($data['jam_keluar']);
    $akhir = strtotime($data['jam_keluar']);


    $data['jam_keluar'] = date('Y-m-d H:i:s',$awal);
    $data['jam_kembali'] = date('Y-m-d H:i:s',$akhir);
    $m_user_id = $data['m_user_id']['master_user_id'];
    $siswa = $db->find('select id from master_siswa where master_user_id_siswa = "' . $_SESSION['user']['id'] . '"');

    $data['master_siswa_id'] = $siswa->id;
    $data['master_user_id'] = $m_user_id;



    $validasi = validasi($data);


    if ($validasi === TRUE) {
        try {
            $model = $db->insert('transaksi_perizinan', $data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }





});
