<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        'master_siswa_id'       => 'required',

    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get('/apppelanggaran/siswa/{kelas}', function ($request, $response) {
    $db = $this->db;

    $kelas = $request->getAttribute('kelas');


    try {
        $model =  $db->select("master_siswa.*")
            ->from("master_siswa")
            ->where("master_kelas_id","=",$kelas)
            ->findAll();
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal disimpan']);
    }


});

/**
 * get user detail for update profile
 */
$app->get('/apppelanggaran/view', function ($request, $response) {
    $db = $this->db;

    $data = $db->find('select id, nama, username, m_roles_id from m_user where id = "' . $_SESSION['user']['id'] . '"');

    return successResponse($response, $data);
});
/**
 * get user list
 */
$app->get('/apppelanggaran/index', function ($request, $response) {
    $params = $_REQUEST;

    $sort   = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit  = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    $db->select("master_pelanggaran.*,master_siswa.nama as namasiswa")
    ->from('master_pelanggaran')
    ->leftJoin("master_siswa","master_siswa.id = master_pelanggaran.master_siswa_id");

    /** set parameter */
//    if (isset($params['filter'])) {
//        $filter = (array) json_decode($params['filter']);
//        foreach ($filter as $key => $val) {
//          if ($key == 'is_deleted') {
//            $db->where('master_user.is_deleted', '=', $val);
//          }
//          elseif ($key == 'username') {
//            $db->andWhere('m_user.username', 'LIKE', $val);
//          } elseif ($key == 'nama') {
//            $db->andWhere('m_user.nama', 'LIKE', $val);
//          }
//        }
//    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models    = $db->findAll();



    $totalItem = $db->count();

    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

$app->get('/apppelanggaran/caripelanggaransiswa', function ($request, $response) {
    $params = $_REQUEST;
    $db = $this->db;

    $data = $db->select("transaksi_pelanggaran.*,master_siswa.nama as nama_siswa")
        ->from('transaksi_pelanggaran')
        ->leftJoin("master_siswa","master_siswa.id = transaksi_pelanggaran.m_siswa_id")
        ->where("m_siswa_id","=",$params['id'])
        ->findAll();

    if (!empty($data)){
        return successResponse($response, ['data' => $data]);
    }else{
        return unprocessResponse($response,['Siswa ini belum memiliki pelanggaran']);
    }

});

/**
 * create user
 */
$app->post('/apppelanggaran/save', function ($request, $response) {
    $data = $request->getParams();
    $data["file"] = "";
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $attachment["body"] = $request->getUploadedFiles()["attachment"];
            if (!empty($attachment["body"])) {
                $attachment["name_file"] = $attachment["body"]->getClientFilename(); 
                $attachment["body"]->moveTo("storage/attachment_pelanggaran/{$attachment['name_file']}");
                $data["file"] = $attachment["name_file"];
            }
            $model = $db->insert("transaksi_pelanggaran", $data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e->getMessage());
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * update user profile
 */


/**
 * update user
 */
$app->post('/apppelanggaran/update', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            $model = $db->update("master_pelanggaran", $data, array('id' => $data['id']));
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * delete user
 */
$app->delete('/apppelanggaran/delete/{id}', function ($request, $response) {
    $db = $this->db;

    $file = $db->select("*")
                ->from("master_pelanggaran")
                ->where("id","=",$request->getAttribute('id'))
                ->find();
    try {
        unlink("../storage/attachment_pelanggaran/".$file->file);
        $delete = $db->delete('master_pelanggaran', array('id' => $request->getAttribute('id')));


        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }
});
