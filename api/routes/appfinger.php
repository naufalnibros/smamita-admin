<?php

/**
 * Validasi Input
 * @param array $custom
 * @return void
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        'nama' => 'required',
        'ip' => 'required',
        'port' => 'required',
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Get list
 */
$app->get('/appfinger/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("*")
        ->from("master_finger");

    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array)json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", "%$val%");
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();

    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save
 */
$app->post('/appfinger/save', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            if (!empty($data["id"])) {
                $model = $db->update("master_finger", $data, ['id' => $data['id']]);
            } else {
                $model = $db->insert("master_finger", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [ "data gagal disimpan {$e->getMessage()}"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appfinger/delete/{id}', function ($request, $response) {
    $db = $this->db;
    try {
        $db->delete("master_finger", array('id' => $request->getAttribute('id')));
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }
    
});