<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/transaksi_finger/index', function (Request $request, Response $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 50;

    $db = $this->db;

    /** Select data finger from database */
    $db->select("transaksi_finger.*, master_siswa.nama as namasiswa")
        ->from("transaksi_finger")
        ->leftJoin("master_siswa", "master_siswa.nipd = transaksi_finger.unique_id");

    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array)json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == "namasiswa") {
                $db->where("master_siswa.nama", "LIKE", "%$val%");
            } else if ($key == "unique_id") {
                $db->where($key, "LIKE", "%$val%");
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();

    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

$app->get("/transaksi_finger/mesin_finger", function (Request $request, Response $response) {
    $db = $this->db;
    return successResponse($response, $db->findAll("SELECT * FROM master_finger ORDER BY id DESC"));
});


// $app->get("/transaksi_finger/finger/{ip}", function (Request $request, Response $response, $args) {
//     try {
//         $finger_models = new ZKLibrary($args["ip"], 4370);

//         $finger_models->connect();
//         $finger_models->disableDevice();
    
//         // $users = $finger_models->getUser();
//         $attendances = $finger_models->getAttendance();
    
//         $finger_models->enableDevice();
//         $finger_models->disconnect();

//         return successResponse($response, [
//             $attendances
//         ]);
//     } catch (\Throwable $e) {
//         return unprocessResponse($response, $e->getMessage());
//     }

// })->setName("tarik-data-finger");

function Parse_Data ($data,$p1,$p2) {
    $data = " ".$data;
    $hasil = "";
    $awal = strpos($data,$p1);
    if ($awal != "") {
      $akhir = strpos(strstr($data,$p1),$p2);
      if ($akhir != ""){
        $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
      }
    }
    return $hasil; 
  }

$app->get("/transaksi_finger/finger/{ip}", function(Request $request, Response $response, $args){
    $Key="0";
    
    $models = array();
    
        $Connect = fsockopen($args["ip"], "80", $errno, $errstr, 1);
        if($Connect){
            $soap_request="<GetAttLog>
                                <ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey>
                                <Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg>
                            </GetAttLog>";
         
         
    
            $newLine="\r\n";
            fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
            fputs($Connect, "Content-Type: text/xml".$newLine);
    
     
    
            fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
            fputs($Connect, $soap_request.$newLine);
            $buffer="";
            while($Response=fgets($Connect, 1024)){
                $buffer=$buffer.$Response;
            }
        } else {
            return unprocessResponse($response, "Koneksi Tidak ditemukan");
        }
     
        $buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
        $buffer=explode("\r\n",$buffer);


        for($a=0;$a<count($buffer);$a++){
            $data=Parse_Data($buffer[$a],"<Row>","</Row>");
         
            $pin=Parse_Data($data,"<PIN>","</PIN>");
            $datetime=Parse_Data($data,"<DateTime>","</DateTime>");
            $status=Parse_Data($data,"<Status>","</Status>");
    
            if (!empty($pin)) {
                $models[] = array(
                    "unique_id" => $pin,
                    "datetime" => $datetime,
                    "status" => $status
                );
            }
            ini_set('max_execution_time', 300);
        }

        return successResponse($response, $models);

})->setName("tarik-data-finger");


$app->get("/transaksi_finger/syncronize", function(Request $request, Response $response){
    $params = $request->getParams();

    if (empty($params["ip"])) return unprocessResponse($response, "Pilih Mesin Finger terlebih dahulu");

    $db = $this->db;

    try {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => getenv("URL_REQUEST_FINGER") . $params["ip"],
            // CURLOPT_URL => getenv("SAMPLE_JSON_FINGER"),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $dataJson = json_decode(curl_exec($curl), TRUE);
        curl_close($curl);

        if ($dataJson["status_code"] != 200) {
            return unprocessResponse($response, $dataJson["errors"]);
        }

        /** Log Data Finger */
        // return successResponse($response, ["data_finger" => $dataJson["data"], "url" => getenv("URL_REQUEST_FINGER") . $params["ip"]]);

        $models = $db_models = array();

        foreach ($dataJson["data"] as $key => $value) {
            if (date("Y-m-d", strtotime($value["datetime"])) == date("Y-m-d", strtotime($params["tanggal"]))) {
                $models[$key] = (array) $value;
                $models[$key]["type_user"] = "siswa";   
                
                $find = $db->select("*")
                ->from("transaksi_finger")
                ->andWhere("unique_id", "=", $value["unique_id"])
                ->andWhere("status", "=", $value["status"])
                ->andWhere("datetime", "=", $value["datetime"])
                ->find();

                if (empty($find)) {
                    $db_models[] = $db->run(
                        "REPLACE INTO transaksi_finger 
                    (unique_id, status, datetime) VALUES (?, ?, ?)",
                        [$value["unique_id"], $value["status"], $value["datetime"]]
                    );
                }
            }
        }

        if (empty($db_models)) return unprocessResponse($response, "Data Finger telah diperbarui sebelumya : " . date("d F Y", strtotime($params["tanggal"])) );
        
        if (empty($models)) return unprocessResponse($response, "Data Finger tidak ditemukan pada : " . date("d F Y", strtotime($params["tanggal"])) );

        return successResponse($response, $models);

    } catch (\Exception $e) {
        return unprocessResponse($response, $e->getMessage());
    }
    
})->setName("tarik-data-finger");

$app->get("/transaksi_finger/absensi_siswa", function(Request $request, Response $response){

    $db = $this->db;

    $models = $db->findAll("SELECT * FROM master_siswa");

    foreach ($models as $key => $value) {
        $models[$key] = (array) $value;
        $models[$key]["list_absensi"] = $db->findAll("SELECT * FROM transaksi_finger WHERE unique_id = $value->nipd");
    }

    return successResponse($response, $models);

})->setName("tarik-data-finger");

/**
 * http://localhost/smamita/api/transaksi_finger/absensi/siswa/11152
 */
 function to_time($datetime){
    return strtotime(date("H:i:s", strtotime($datetime)));
}

/**
 * Keterangan Absen
 *
 * @param Object $datang
 * @param Object $pulang
 * @return String
 */
function keterangan($datang, $pulang){
    $jam_datang = array(
        "awal" => strtotime("05:00:00"),
        "akhir" => strtotime("07:00:00"),
    );
    $jam_pulang = array(
        "awal" => strtotime("15:30:00"),
        "akhir" => strtotime("17:00:00"),
    );
    if (!$pulang) {
        return "TANPA KETERANGAN";
    }
    if (($jam_datang["awal"] <= to_time($datang->datetime)) && (to_time($datang->datetime) <=  $jam_datang["akhir"])) {
        if (!(($jam_pulang["awal"] <= to_time($pulang->datetime)) && (to_time($pulang->datetime) <=  $jam_pulang["akhir"]))) {
            if (!($jam_pulang["awal"] <= to_time($pulang->datetime))) { return "PULANG AWAL"; }
            return "TANPA KETERANGAN";
        }
    } else {
        return "TERLAMBAT";
    }
    return "MASUK";
}

$app->get("/transaksi_finger/absensi/siswa/{nipd}", function(Request $request, Response $response, $args){
    date_default_timezone_set('Asia/Jakarta');
    $models = array();
    $db = $this->db;

    if ($args["nipd"] == "session") {
        $args["nipd"] = $db->select("nipd")->from("master_siswa")->where("master_user_id_siswa", "=", $_SESSION['user']['id'])->find()->nipd;
    }

    $datang = $db->select("*")
                ->from("transaksi_finger")
                ->where("unique_id", "=", $args["nipd"])
                ->where("status", "=", 0)              
                ->findAll();

    foreach ($datang as $value) {

        $pulang = $db->select("*")
            ->from("transaksi_finger")
            ->where("unique_id", "=", $args["nipd"])
            ->where("status", "=", 1)
            ->where("DATE(datetime)", "=", date("Y-m-d", strtotime($value->datetime)))
            ->find();

        $models[] = array(
            "datang" => $value,
            "pulang" => $pulang,
            "tanggal" => tgl_indo(strtotime($value->datetime)),
            "jam_masuk" => date("H:i:s", strtotime($value->datetime)),
            "jam_pulang" => (!$pulang) ? "-" : date("H:i:s", strtotime($pulang->datetime)),
            "keterangan" => keterangan($value, $pulang)
        );
    }
    return successResponse($response, $models);
});