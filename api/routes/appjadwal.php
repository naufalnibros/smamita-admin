<?php
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'nama' => 'required',

    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Get list user roles
 */

$app->get('/appjadwal/kelas', function (Request $request, Response $response) {

    $db = $this->db;
        try {
           $model =  $db->select("master_kelas.*")
                ->from("master_kelas")
                ->findAll();
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }


});
$app->get('/appjadwal/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("master_jadwal.*,master_kelas.kelas as namakelas")
            ->from("master_jadwal")
            ->leftJoin("master_kelas","master_kelas.id = master_jadwal.master_kelas_id");


    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
              $db->where('master_jadwal.nama', 'LIKE', $val);
            }elseif ($key == "kelas"){
                $db->andWhere("master_kelas.kelas","LIKE",$val);
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();


    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save roles
 */
$app->post('/appjadwal/save', function (Request $request, Response $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);
    
    if ($validasi === true) {
        try {
            if (isset($request->getUploadedFiles()["attachment"])){
                $attachment["body"] = $request->getUploadedFiles()["attachment"];
                $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                // Menyimpan File ke directory tertentu
                $attachment["body"]->moveTo("storage/attachment_jadwal/{$attachment['name_file']}");

                $data["attachment"] = $attachment["name_file"];
            }

            $model = $db->insert("master_jadwal", $data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [ "data gagal disimpan {$e->getMessage()}"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appjadwal/delete/{id}', function ($request, $response) {
    $db = $this->db;
    $id = $request->getAttribute('id');

    $file = $db->select("*")
        ->from("master_jadwal")
        ->where("id","=",$id)
        ->find();
    $filename= $file->attachment;
    try {
        if (isset($filename)){
            $path = 'storage/attachment_jadwal/'.$filename;
            unlink($path);
        }
        $delete = $db->delete('master_jadwal', array('id' => $id));
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }
});

$app->post('/appjadwal/update', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);



    if ($validasi === true) {
        try {

            if (isset($request->getUploadedFiles()["attachment"])){

                $file = $db->select("*")
                    ->from("master_jadwal")
                    ->where("id","=",$data["id"])
                    ->find();
                $filename= $file->attachment;

                if (isset($filename)){
                    $path = 'storage/attachment_jadwal/'.$filename;
                    unlink($path);
                    $attachment["body"] = $request->getUploadedFiles()["attachment"];
                    $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                    // Menyimpan File ke directory tertentu
                    $attachment["body"]->moveTo("storage/attachment_jadwal/{$attachment['name_file']}");
                    $data["attachment"] = $attachment["name_file"];
                }else{
                    $attachment["body"] = $request->getUploadedFiles()["attachment"];
                    $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                    // Menyimpan File ke directory tertentu
                    $attachment["body"]->moveTo("storage/attachment_jadwal/{$attachment['name_file']}");
                    $data["attachment"] = $attachment["name_file"];
                }

                $model = $db->update("master_jadwal", $data, array('id' => $data['id']));
                return successResponse($response, $model);
            }else{

                $model = $db->update("master_jadwal", $data, array('id' => $data['id']));
                return successResponse($response, $model);
            }
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});
