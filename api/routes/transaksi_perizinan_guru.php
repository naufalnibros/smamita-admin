<?php

$app->get('/import/format_furu', function ($request, $response) { });


$app->get('/transaksi_perizinan_guru/listPerizinan', function ($request, $response) {
    $db = $this->db;


    $id = $_SESSION['user']['id'];
    $data = $db->select('transaksi_perizinan.*,master_user.nama, master_siswa.nama as namasiswa')
        ->from('transaksi_perizinan')
        ->leftJoin("master_user","master_user.id = transaksi_perizinan.master_user_id ")
        ->leftJoin("master_siswa","master_siswa.id = transaksi_perizinan.master_siswa_id")
        ->where('master_user_id','=',$id)
        ->findAll();
    return successResponse($response, $data);
});

$app->post('/transaksi_perizinan_guru/setujui', function ($request, $response) {

    $params = $request->getParams();
    $db = $this->db;


    try {
        $data = $db->update("transaksi_perizinan",$params, ['id' => $params['id']]);
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }


    return successResponse($response, $data);
});

$app->post('/transaksi_perizinan_guru/kembali', function ($request, $response) {

    $params = $request->getParams();
    $db = $this->db;


    try {
        $data = $db->update("transaksi_perizinan",$params, ['id' => $params['id']]);
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }


    return successResponse($response, $data);
});