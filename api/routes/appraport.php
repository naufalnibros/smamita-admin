<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'semester' => 'required',

    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Get list user roles
 */

$app->get('/appraport/kelas', function ($request, $response) {

    $db = $this->db;
        try {
           $model =  $db->select("master_kelas.*")
                ->from("master_kelas")
                ->findAll();
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }


});
$app->get('/appraport/siswa/{kelas}', function ($request, $response) {
    $db = $this->db;

    $kelas = $request->getAttribute('kelas');


    try {
        $model =  $db->select("master_siswa.*")
            ->from("master_siswa")
            ->where("master_kelas_id","=",$kelas)
            ->findAll();
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal disimpan']);
    }


});
$app->get('/appraport/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("master_raport.*,master_siswa.nama as namasiswa,master_kelas.kelas as namakelas")
            ->from("master_raport")
            ->leftJoin("master_siswa","master_siswa.id = master_raport.master_siswa_id")
            ->leftJoin("master_kelas","master_kelas.id = master_raport.master_kelas_id");


    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
              $db->where('master_jadwal.nama', 'LIKE', $val);
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();


    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save roles
 */
$app->post('/appraport/save', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;


    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            if (isset($request->getUploadedFiles()["attachment"])){
                // Mengambil File dan Mengambil nama
                $attachment["body"] = $request->getUploadedFiles()["attachment"];
                $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                // Menyimpan File ke directory tertentu
                $attachment["body"]->moveTo("storage/attachment_raport/{$attachment['name_file']}");
                $data["attachment"] = $attachment["name_file"];
            }
            $model = $db->insert("master_raport",$data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appraport/delete/{id}', function ($request, $response) {
    $db = $this->db;
    $id = $request->getAttribute('id');
    try {

        $file = $db->select("*")
            ->from("master_raport")
            ->where("id","=",$id)
            ->find();
        $filename= $file->attachment;
        if (isset($filename)){
            $path = 'storage/attachment_raport/'.$filename;
            unlink($path);
        }

        $delete = $db->delete('master_raport', array('id' => $id));
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }
});

$app->post('/appraport/update', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {

            if (isset($request->getUploadedFiles()["attachment"])){

                $file = $db->select("*")
                    ->from("master_raport")
                    ->where("id","=",$data["id"])
                    ->find();
                $filename= $file->attachment;

                if (isset($filename)){
                    $path = 'storage/attachment_raport/'.$filename;
                    unlink($path);
                    $attachment["body"] = $request->getUploadedFiles()["attachment"];
                    $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                    // Menyimpan File ke directory tertentu
                    $attachment["body"]->moveTo("storage/attachment_raport/{$attachment['name_file']}");
                    $data["attachment"] = $attachment["name_file"];
                }else{
                    $attachment["body"] = $request->getUploadedFiles()["attachment"];
                    $attachment["name_file"] = $attachment["body"]->getClientFilename(); // Mas rahmat nanti Format nama yg disimpan : Y-m-d-h-i-s

                    // Menyimpan File ke directory tertentu
                    $attachment["body"]->moveTo("storage/attachment_raport/{$attachment['name_file']}");
                    $data["attachment"] = $attachment["name_file"];
                }

                $model = $db->update("master_raport", $data, array('id' => $data['id']));
                return successResponse($response, $model);
            }else{

                $model = $db->update("master_raport", $data, array('id' => $data['id']));
                return successResponse($response, $model);
            }




        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

