<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'telepon' => 'required',
        'nipd' => 'required',
        'telepon_ortu' => 'required',
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Get list user roles
 */
$app->get('/appsiswa/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("master_siswa.*,master_kelas.kelas as namakelas")
            ->from("master_siswa")
            ->leftJoin("master_kelas","master_kelas.id =master_siswa.master_kelas_id ");

    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'kelas') {
              $db->where('master_kelas.kelas', 'LIKE', "%{$val}%");
            } elseif ($key == 'jenjang') {
              $db->andWhere('master_kelas.jenjang', '=',"%{$val}%");
            } elseif ($key == 'jurusan') {
                $db->andWhere('master_kelas.jurusan', 'LIKE',"%{$val}%");
            } elseif ($key == "nipd") {
                $db->andWhere('master_siswa.nipd', 'LIKE',"%{$val}%");
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();

    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save roles
 */
$app->post('/appsiswa/save', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    $data["tanggal_lahir"] = date("Y-m-d", strtotime($data["tanggal_lahir"]));

    if ($validasi === true) {
        try {

            $a = insertSiswa($data);

            return successResponse($response, $a);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->post('/appsiswa/update', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    $data["tanggal_lahir"] = date("Y-m-d", strtotime($data["tanggal_lahir"]));

    if ($validasi === true) {
        try {

            $model = $db->update('master_siswa', $data, array('id' => $data['id']));

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appsiswa/delete/{id}', function ($request, $response) {
    $db = $this->db;
    $id = $request->getAttribute('id');

    $data = $db->find("select * from master_siswa where id='{$id}'");
    $idsiswa = $data->master_user_id_siswa;
    $idwali = $data->master_user_id_walimurid;

      try {
        $delete = $db->delete('master_siswa', array('id' => $id));
        $delete_user_siswa =$db->run("delete from master_user where id = '" . $idsiswa . "'");
        $delete_user_wali = $db->run("delete from master_user where id = '" . $idwali . "'");;
        return successResponse($response, ['data berhasil dihapus']);
      } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
      }

});


