<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'nip' => 'required',
        'telepon' => 'required',
        'nama' => 'required',
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Get list user roles
 */
$app->get('/appguru/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("master_guru.*,master_user.telepon")
            ->from("master_guru")
            ->leftJoin("master_user","master_user.id = master_guru.master_user_id");


    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'nip') {
              $db->where('master_guru.nip', 'LIKE', $val);
            } elseif ($key == 'nama') {
              $db->andWhere('master_guru.nama', 'LIKE', $val);
            }elseif ($key == "telepon"){
                $db->andWhere('master_user.telepon','LIKE',$val);
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    $db->orderBy($sort);

    $models = $db->findAll();

    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save roles
 */
$app->post('/appguru/save', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            $datauser['nama'] = $data['nama'];
            $datauser['username'] = $data['nip'];
            $datauser['password'] = sha1($data['nip']);
            $datauser['telepon'] = (isset($data['telepon']) ? $data['telepon'] : '');
            $datauser['type'] = "guru";
            $datauser["master_role_id"] = 2;

            $modeluser = $db->insert("master_user",$datauser);

            $dataguru['nip'] = $data['nip'];
            $dataguru['nama'] = $data['nama'];
            $dataguru['master_user_id'] = $modeluser->id;
            $modelguru = $db->insert('master_guru', $dataguru);


            return successResponse($response, $modelguru);
        } catch (Exception $e) {
            return unprocessResponse($response, "Terjadi Kesalahan");
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appguru/delete/{id}', function ($request, $response) {
    $db = $this->db;
    $id = $request->getAttribute('id');

    $data_guru = $db->select("*")->from("master_guru")->where("id", "=", $id)->find();

    try {
        $db->delete("master_user", ["username" => $data_guru->nip, "type" => "guru"]);
        $db->delete('master_guru', array('id' => $id));
        return successResponse($response, ['data berhasil dihapus']);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal dihapus']);
    }
});

$app->post('/appguru/update', function ($request, $response) {
    $data = $request->getParams();

    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            $data['akses'] = json_encode($data['akses']);
            $model = $db->update("m_roles", $data, array('id' => $data['id']));

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});
