<?php

use Slim\Http\Request;
use Slim\Http\Response;


function saveGuru($db, $model){

    $master_user = $db->insert("master_user", [
        "nama" => $model["nama"],
        "username" => $model["nip"],
        "telepon" => $model["telepon"],
        "type" => "guru",
        "master_role_id" => 2, // Role id Guru
        "password" => sha1($model["nip"])
    ]);

    $model["master_user_id"] = $master_user->id;

    $db->insert("master_guru", $model);
}

function saveSiswa($db, $model){

    $master_user_siswa = $db->insert("master_user", [
        "nama" => $model["nama"],
        "username" => $model["nipd"],
        "telepon" => (empty($model["telepon"]) ? $model["nipd"] : $model["telepon"]),
        "type" => "siswa",
        "master_role_id" => 4, // Role id Siswa
        "password" => sha1($model["nipd"])
    ]);

    $master_user_walimurid = $db->insert("master_user", [
        "nama" => "Wali Murid : {$model['nama']}",
        "username" => (empty($model["telepon_ortu"]) ? "walimurid_".$model["nipd"] : $model["telepon_ortu"]),
        "telepon" => (empty($model["telepon_ortu"]) ? "Belum Diisi" : $model["telepon_ortu"]),
        "type" => "walimurid",
        "master_role_id" => 3, // Role id WAimurid
        "password" => sha1( (empty($model["telepon_ortu"]) ? "walimurid_".$model["nipd"] : $model["telepon_ortu"]) )
    ]);

    $kelas_siswa = $db->find("SELECT * FROM master_kelas WHERE kelas = '{$model['kelas_siswa']}'");

    $model["telepon"] = (empty($model["telepon"]) ? $model["nipd"] : $model["telepon"]);
    $model["telepon_ortu"] = (empty($model["telepon_ortu"]) ? "Belum Diisi" : $model["telepon_ortu"]);

    $model["master_kelas_id"] = empty($kelas_siswa) ? 0 : $kelas_siswa->id;
    $model["master_user_id_siswa"] = $master_user_siswa->id;
    $model["master_user_id_walimurid"] = $master_user_walimurid->id;

    $db->insert("master_siswa", $model);
}

function saveFinger($db, $model){
    foreach ($model as $key => $value) {
        // if (date("Y-m-d", strtotime($value[3])) === date("Y-m-d")) {
            $QUERY = "SELECT * FROM transaksi_finger WHERE unique_id = {$value["unique_id"]} AND status = {$value["status"]} AND datetime = '{$value["datetime"]}' ";
            $find = $db->find($QUERY);
            if (empty($find)) {
                $db_models[] = $db->run(
                    "REPLACE INTO transaksi_finger 
                    (unique_id, status, datetime) VALUES (?, ?, ?)",
                    [$value["unique_id"], $value["status"], $value["datetime"]]
                );
            }
        // }
    }
}

$app->post('/import/finger', function (Request $request, Response $response) {
    $db = $this->db;
    $models = array();

    if (!empty($_FILES)) {

        foreach ($_FILES as $key => $value) {
            $tempPath = $value['tmp_name'];
            $newName  = urlParsing($value['name']);

            $inputFileName = "./upload" . DIRECTORY_SEPARATOR . rand() . "_" . $newName;
            move_uploaded_file($tempPath, $inputFileName);

            if (file_exists($inputFileName)) {
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel   = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }

                $sheet         = $objPHPExcel->getSheet(0);
                $highestRow    = $sheet->getHighestRow();

                if (($objPHPExcel->getSheet(0)->getCell("A" . 1)->getValue() !== "unique_id")
                    || ($objPHPExcel->getSheet(0)->getCell("B" . 1)->getValue() !== "status")
                    || ($objPHPExcel->getSheet(0)->getCell("C" . 1)->getValue() !== "datetime")
                ) {
                    unlink($inputFileName);
                    return unprocessResponse($response, "Format yang Anda Masukkan salah");
                }

                try {
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $models[] = array(
                            "unique_id" => $objPHPExcel->getSheet(0)->getCell("A" . $row)->getValue(),
                            "status" => $objPHPExcel->getSheet(0)->getCell("B" . $row)->getValue(),
                            "datetime" => $objPHPExcel->getSheet(0)->getCell("C" . $row)->getValue()
                        );
                    }
                    unlink($inputFileName);
                } catch (\Throwable $th) {
                    unlink($inputFileName);
                    return unprocessResponse($response, $th->getMessage());
                }

                if (empty($models)) {
                    return unprocessResponse($response, "data gagal di import $inputFileName");
                }
            } else {
                return unprocessResponse($response, "data gagal di import $inputFileName");
            }
        }

        try {
            saveFinger($db, $models);
            return successResponse($response, $models);
        } catch (\Exception $e){
            return unprocessResponse($response, $e->getMessage());
        }
    }

    return unprocessResponse($response, "Terjadi Kesalahan");
});

$app->post('/import/guru', function (Request $request, Response $response) {
    $db = $this->db;
    $models = array();

    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName  = urlParsing($_FILES['file']['name']);

        $inputFileName = "./upload" . DIRECTORY_SEPARATOR . rand() . "_" . $newName;
        move_uploaded_file($tempPath, $inputFileName);
        if (file_exists($inputFileName)) {
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
            }

            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();

            if (($objPHPExcel->getSheet(0)->getCell("A" . 1)->getValue() !== "nip")
            || ($objPHPExcel->getSheet(0)->getCell("B" . 1)->getValue() !== "nama")
            || ($objPHPExcel->getSheet(0)->getCell("C" . 1)->getValue() !== "status_kepegawaian")
            || ($objPHPExcel->getSheet(0)->getCell("D" . 1)->getValue() !== "telepon")) {
                unlink($inputFileName);
                return unprocessResponse($response, "Format yang Anda Masukkan salah");
            }

            try {
                for ($row = 2; $row <= $highestRow; $row++) {
                    $models[$row] = array(
                        "nip" => $objPHPExcel->getSheet(0)->getCell("A" . $row)->getValue(),
                        "nama" => $objPHPExcel->getSheet(0)->getCell("B" . $row)->getValue(),
                        "status_kepegawaian" => ($objPHPExcel->getSheet(0)->getCell("C" . $row)->getValue() === "Aktif" ? 1 : 0),
                        "telepon" => $objPHPExcel->getSheet(0)->getCell("D" . $row)->getValue()
                    );
                    saveGuru($db, $models[$row]);
                }
                unlink($inputFileName);
            } catch (\Throwable $th) {
                unlink($inputFileName);
                return unprocessResponse($response, $th->getMessage());
            }

            if (empty($models)) {
                return unprocessResponse($response, "data gagal di import $inputFileName");
            }

            return successResponse($response, $models);
        } else {
            return unprocessResponse($response, "data gagal di import $inputFileName");
        }
    }

    return unprocessResponse($response, "Terjadi Kesalahan");
});

$app->post('/import/siswa', function (Request $request, Response $response) {
    $db = $this->db;
    $models = array();

    if (!empty($_FILES)) {
        $tempPath = $_FILES['file']['tmp_name'];
        $newName  = urlParsing($_FILES['file']['name']);

        $inputFileName = "./upload" . DIRECTORY_SEPARATOR . rand() . "_" . $newName;
        move_uploaded_file($tempPath, $inputFileName);

        if (file_exists($inputFileName)) {
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
            }

            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();

            if (($objPHPExcel->getSheet(0)->getCell("A" . 1)->getValue() !== "nipd")
            || ($objPHPExcel->getSheet(0)->getCell("B" . 1)->getValue() !== "nama")
            || ($objPHPExcel->getSheet(0)->getCell("C" . 1)->getValue() !== "tempat_lahir")
            || ($objPHPExcel->getSheet(0)->getCell("D" . 1)->getValue() !== "tanggal_lahir")
            || ($objPHPExcel->getSheet(0)->getCell("E" . 1)->getValue() !== "jenis_kelamin")
            || ($objPHPExcel->getSheet(0)->getCell("F" . 1)->getValue() !== "agama")
            || ($objPHPExcel->getSheet(0)->getCell("G" . 1)->getValue() !== "alamat")
            || ($objPHPExcel->getSheet(0)->getCell("H" . 1)->getValue() !== "telepon")
            || ($objPHPExcel->getSheet(0)->getCell("I" . 1)->getValue() !== "telepon_ortu")
            || ($objPHPExcel->getSheet(0)->getCell("J" . 1)->getValue() !== "nama_ayah")
            || ($objPHPExcel->getSheet(0)->getCell("K" . 1)->getValue() !== "nama_ibu")
            || ($objPHPExcel->getSheet(0)->getCell("L" . 1)->getValue() !== "tahun_masuk")
            || ($objPHPExcel->getSheet(0)->getCell("M" . 1)->getValue() !== "kelas_siswa")) {
                unlink($inputFileName);
                return unprocessResponse($response, "Format yang Anda Masukkan salah");                
            }
 
            try {
                for ($row = 2; $row <= $highestRow; $row++) {
                    $models[$row] = array(
                        "nipd" => $objPHPExcel->getSheet(0)->getCell("A" . $row)->getValue(),
                        "nama" => $objPHPExcel->getSheet(0)->getCell("B" . $row)->getValue(),
                        "tempat_lahir" => $objPHPExcel->getSheet(0)->getCell("C" . $row)->getValue(),
                        "tanggal_lahir" => $objPHPExcel->getSheet(0)->getCell("D" . $row)->getValue(),
                        "jenis_kelamin" => $objPHPExcel->getSheet(0)->getCell("E" . $row)->getValue(),
                        "agama" => $objPHPExcel->getSheet(0)->getCell("F" . $row)->getValue(),
                        "alamat" => $objPHPExcel->getSheet(0)->getCell("G" . $row)->getValue(),
                        "telepon" => $objPHPExcel->getSheet(0)->getCell("H" . $row)->getValue(),
                        "telepon_ortu" => $objPHPExcel->getSheet(0)->getCell("I" . $row)->getValue(),
                        "nama_ayah" => $objPHPExcel->getSheet(0)->getCell("J" . $row)->getValue(),
                        "nama_ibu" => $objPHPExcel->getSheet(0)->getCell("K" . $row)->getValue(),
                        "tahun_masuk" => $objPHPExcel->getSheet(0)->getCell("L" . $row)->getValue(),
                        "kelas_siswa" => $objPHPExcel->getSheet(0)->getCell("M" . $row)->getValue()
                    );
                    saveSiswa($db, $models[$row]);
                }
                unlink($inputFileName);
            } catch (\Throwable $th) {
                unlink($inputFileName);
                return unprocessResponse($response, $th->getMessage());
            }

            if (empty($models)) {
                return unprocessResponse($response, "data gagal di import $inputFileName, data yang diinputkan kosong");
            }

            return successResponse($response, $models);
        } else {
            return unprocessResponse($response, "data gagal di import $inputFileName ");
        }
    }

    return unprocessResponse($response, "Terjadi Kesalahan");
});