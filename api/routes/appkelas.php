<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'kelas' => 'required',
        'jenjang' => 'required',
        'jurusan' => 'required',
    );

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Get list user roles
 */
$app->get('/appkelas/index', function ($request, $response) {
    $params = $request->getParams();

    $sort = "id DESC";
    $offset = isset($params['offset']) ? $params['offset'] : 0;
    $limit = isset($params['limit']) ? $params['limit'] : 10;

    $db = $this->db;

    /** Select roles from database */
    $db->select("*")
            ->from("master_kelas");

    /** Add filter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'kelas') {
              $db->where('master_kelas.kelas', 'LIKE', $val);
            } elseif ($key == 'jenjang') {
              $db->andWhere('master_kelas.jenjang', 'LIKE', $val);
            }elseif ($key == 'jurusan') {
                $db->andWhere('master_kelas.jurusan', 'LIKE', $val);
            }
        }
    }

    /** Set limit */
    if (!empty($limit)) {
        $db->limit($limit);
    }

    /** Set offset */
    if (!empty($offset)) {
        $db->offset($offset);
    }

    /** Set sorting */
    if (!empty($params['sort'])) {
        $db->sort($sort);
    }

    $models = $db->findAll();

    $totalItem = $db->count();
    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * Save roles
 */
$app->post('/appkelas/save', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {

            $model = $db->insert("master_kelas",$data);

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});
$app->post('/appkelas/update', function ($request, $response) {



    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);

    if ($validasi === true) {
        try {

            $model = $db->update('master_kelas', $data, array('id' => $data['id']));

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * Delete roles
 */
$app->delete('/appkelas/delete/{id}', function ($request, $response) {
    $db = $this->db;
    $id = $request->getAttribute('id');

      try {
        $delete = $db->delete('master_kelas', array('id' => $id));
        return successResponse($response, ['data berhasil dihapus']);
      } catch (Exception $e) {
        return unprocessResponse($response, ["Data Kelas Masih digunakan untuk Master Jadwal"]);
      }

});


