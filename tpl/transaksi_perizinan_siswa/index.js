app.controller("perizinanSiswaCtrl", function($scope, Data, toaster) {
    var tableStateRef;
    var control_link = "transaksi_perizinan_siswa";
    $scope.displayed = [];

    $scope.form = {};
    $scope.is_view = false;

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = mm + '/' + dd + '/' + yyyy + ' ' + today.getHours() + ":" + today.getMinutes();

    $scope.form.jam_keluar = new Date(today);
    $scope.form.jam_kembali = new Date(today);


    Data.get('appguru/index').then(function(response) {
        $scope.list_guru = response.data.list;
    });


    $scope.getPerizinanSiswa = function() {
        Data.get('transaksi_perizinan_siswa/listPerizinan').then(function(response) {
            $scope.list_izin = response.data;
        });
    };

    $scope.getPerizinanSiswa();

    /** view data */
    $scope.view = function(form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Lihat Data : " + form.nama;
        $scope.form = form;
    };

    /** save request */
    $scope.save = function(form) {
        Data.post(`${control_link}/save`, form).then(function(result) {
            if (result.status_code == 200) {
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
                $scope.getPerizinanSiswa();
                $scope.form.keterangan = "";
                $scope.form.jam_keluar = new Date(today);
                $scope.form.jam_kembali = new Date(today);
            } else {
                toaster.pop('error', "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
        });

    };

})
