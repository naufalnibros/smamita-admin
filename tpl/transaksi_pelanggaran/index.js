app.controller('tpelanggaranCtrl', function($scope, Data, toaster) {
    var tableStateRef;
    var control_link = "apppelanggaran";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = true;
    $scope.is_view = false;

    Data.get('appsiswa/index').then(function(response) {
        $scope.getBarang = response.data.list;
    });

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };

    $scope.getPelanggaranSiswa = function (siswa_id) {
        Data.get(control_link + '/caripelanggaransiswa', siswa_id).then(function(response) {
            if (response.status_code == 200) {
                $scope.is_view = true;
                $scope.displayed = response.data.data;
            } else {
                toaster.pop('error', "Terjadi Kesalahan", setErrorMessage(response.errors));
            }
        });
    };

    /** create action */
    $scope.create = function(form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Form Tambah Data";
        $scope.form = {};
        $scope.getKelas();
    };
    /** update action */
    $scope.update = function(form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.getKelas();
        $scope.getSiswa(form.id);
    };
    /** view data */
    $scope.view = function(form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Lihat Data : " + form.nama;
        $scope.form = form;
    };

    $scope.opened = {};
    $scope.toggle = function($event, elemId) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened[elemId] = !$scope.opened[elemId];
    };

    /** save request */
    $scope.save = function(form) {
        form.m_siswa_id = form.master_siswa_id.id;
        var url = (form.id > 0) ? '/update' : '/save';
        Data.postWithFile(control_link + url, form).then(function(result) {
            if (result.status_code == 200) {
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
                $scope.getPelanggaranSiswa(form.master_siswa_id);
                $scope.form.keterangan_pelanggaran = "";
                $scope.form.attachment = null;
            } else {
                toaster.pop('error', "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
        });

    };
    /** cancel */
    $scope.cancel = function() {
        $scope.callServer(tableStateRef);
        $scope.is_edit = false;
        $scope.is_view = false;
    };


    $scope.delete = function (row) {
      swal({
        title: "Peringatan",
        text: "Anda Akan Menhapus Ini",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya,di hapus",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {
          Data.delete(control_link + '/delete/' + row.id).then(function(result) {
            if (result.status_code == 200) {
              $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            } else {
              swal("Error", result.errors, "error");
            }
          });
          swal("Terhapus", "Data terhapus.", "success");
        } else {
          swal("Error", "Data belum terhapus", "error");
        }
      });
    };
    /** checkAll */

})
