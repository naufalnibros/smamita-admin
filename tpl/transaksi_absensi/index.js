app.controller("transaksiAbsensiCtrl", function ($scope, Data, toaster) {
    var tableStateRef;
    var control_link = "appsiswa";
    $scope.displayed = [];
    $scope.form = {};
    $scope.selectedIndex = {};
    $scope.list_absensi = [];
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        /** set offset and limit */
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        /** set sort and order */
        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        /** set filter */
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }
        Data.get(control_link + '/index', param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
    };

    $scope.getAbsensi = (nipd) => {
        Data.get(`transaksi_finger/absensi/siswa/${nipd}`).then(function (response) {
            $scope.list_absensi = response.data;
        });
    };

    $scope.absensi = function (data, index) {
        $scope.list_absensi = [];
        if (index == $scope.selectedIndex) {
            $scope.selectedIndex = -1;
        } else {
            $scope.selectedIndex = index;
            $scope.getAbsensi(data.nipd);
        }
    }



})