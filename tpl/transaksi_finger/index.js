app.controller("transaksiFingerCtrl", function($scope, Data, toaster) {
  var tableStateRef;
  var control_link = "transaksi_finger";

  $scope.displayed = [];

  $scope.filter = {};
  $scope.filter.tanggal = new Date();
  $scope.filter.ip = null;

  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    /** set offset and limit */
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 50;
    var param = {
      offset: offset,
      limit: limit
    };
    /** set sort and order */
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    /** set filter */
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
  };

  Data.get(control_link + "/mesin_finger").then((result)=>{
    $scope.list_mesin_finger = result.data;
  });

  $scope.opened = {};
  $scope.toggle = function ($event, elemId) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened[elemId] = !$scope.opened[elemId];
  };

  $scope.uploadFiles = function (files) {
    if (files.length > 0) {
      
      Data.postWithFile("import/finger", files).then(function (result) {
        if (result.status_code == 200) {
          $scope.callServer(tableStateRef);
          toaster.pop(
            "success",
            "Berhasil",
            "Data berhasil ditambahkan"
          );
        } else {
          toaster.pop('error', "Terjadi Kesalahan", result.errors);
        }
      });

    }
  };

  $scope.sinkronisasi = (filter) => {
    Data.get(control_link + "/syncronize", filter).then((result) => {
      if (result.status_code == 200) {
        $scope.callServer(tableStateRef);
        toaster.pop("success","Berhasil","Data berhasil ditambahkan");
      } else {
        toaster.pop('error', "Terjadi Kesalahan", result.errors);
      }
    });
  }

  $scope.reset = () => {
    $scope.filter.tanggal = new Date();
    $scope.filter.ip = null; 
  };

});
