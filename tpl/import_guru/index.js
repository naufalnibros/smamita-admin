app.controller('importGuruCtrl', function($scope, Data, toaster, $rootScope) {

    $scope.save = function(form) {
        Data.postWithFile('import/guru', form).then(function(result) {
            if (result.status_code == 200) {
                $scope.is_edit = false;
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
            } else {
                toaster.pop('error', "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
        });
    };
})