angular.module('app').run(
    ['$rootScope', '$state', '$stateParams', 'Data', '$transitions',
        function($rootScope, $state, $stateParams, Data, $transitions) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            /** Pengecekan login */
            $transitions.onStart({}, function($transition$) {
                var toState = $transition$.$to();
                Data.get('site/session').then(function(results) {
                    if (results.status_code == 200) {
                        $rootScope.user = results.data.user;
                        // console.log($rootScope.user)
                        /** Check hak akses */
                      var globalmenu = [
                        'site.dashboard', 
                        'master.userprofile', 
                        'master.setting', 
                        'access.signin', 
                        'laporan.l_artikel', 
                        "transaksi.absensi", 
                        "transaksi.perizinan_siswa", 
                        "transaksi.perizinan_guru"
                      ];
                        if (globalmenu.indexOf(toState.name) >= 0) {} else {
                            if (results.data.user.akses[(toState.name).replace(".", "_")]) {} else {
                                $state.go("access.forbidden");
                            }
                        }
                        /** End */
                    } else {
                        $state.go("access.signin");
                    }
                });


            });
        }
    ]);
angular.module('app').config(function($httpProvider) {
    $httpProvider.interceptors.push(function($q, $rootScope) {
        var numberOfHttpRequests = 0;
        return {
            request: function(config) {
                numberOfHttpRequests += 1;
                $rootScope.waitingForHttp = true;
                return config;
            },
            requestError: function(error) {
                numberOfHttpRequests -= 1;
                $rootScope.waitingForHttp = (numberOfHttpRequests !== 0);
                return $q.reject(error);
            },
            response: function(response) {
                numberOfHttpRequests -= 1;
                $rootScope.waitingForHttp = (numberOfHttpRequests !== 0);
                return response;
            },
            responseError: function(error) {
                numberOfHttpRequests -= 1;
                $rootScope.waitingForHttp = (numberOfHttpRequests !== 0);
                return $q.reject(error);
            }
        };
    });
});
angular.module('app').config(
    ['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/site/dashboard');
            $stateProvider
              .state("site", {
                abstract: true,
                url: "/site",
                templateUrl: "tpl/app.html"
              })
              .state("site.dashboard", {
                url: "/dashboard",
                templateUrl: "tpl/dashboard.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(["chart.js"]).then(function() {
                        return $ocLazyLoad.load("tpl/site/dashboard.js");
                      });
                    }
                  ]
                }
              })
              /** Set default page */
              .state("access", {
                url: "/access",
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
              })
              .state("access.signin", {
                url: "/signin",
                templateUrl: "tpl/page_signin.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/site/site.js").then();
                    }
                  ]
                }
              })
              .state("access.404", {
                url: "/404",
                templateUrl: "tpl/page_404.html"
              })
              .state("access.forbidden", {
                url: "/forbidden",
                templateUrl: "tpl/page_forbidden.html"
              })
              /** End */
              /** Router request master */
              .state("master", {
                url: "/master",
                templateUrl: "tpl/app.html"
              })
              .state("master.setting", {
                url: "/setting",
                templateUrl: "tpl/setting/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/setting/index.js");
                    }
                  ]
                }
              })
              .state("master.userprofile", {
                url: "/profile",
                templateUrl: "tpl/m_user/profil.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/m_user/profil.js");
                    }
                  ]
                }
              })
              .state("master.user", {
                url: "/user",
                templateUrl: "tpl/m_user/user.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/m_user/user.js");
                    }
                  ]
                }
              })
              .state("master.role", {
                url: "/role",
                templateUrl: "tpl/m_roles/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/m_roles/roles.js");
                    }
                  ]
                }
              })
              .state("master.kelas", {
                url: "/kelas",
                templateUrl: "tpl/master_kelas/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/master_kelas/index.js");
                    }
                  ]
                }
              })
              .state("master.guru", {
                url: "/guru",
                templateUrl: "tpl/master_guru/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad
                        .load(["ngFileUpload"])
                        .then(function(params) {
                          return $ocLazyLoad.load("tpl/master_guru/index.js")
                        })
                    }
                  ]
                }
              })
              .state("master.siswa", {
                url: "/siswa",
                templateUrl: "tpl/master_siswa/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function ($ocLazyLoad) {
                      return $ocLazyLoad
                        .load(["ngFileUpload"])
                        .then(function (params) {
                          return $ocLazyLoad.load("tpl/master_siswa/index.js")
                        })
                    }
                  ]
                }
              })
              .state("master.informasi", {
                url: "/informasi",
                templateUrl: "tpl/master_informasi/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/master_informasi/index.js");
                    }
                  ]
                }
              })
              .state("master.prestasi", {
                url: "/prestasi",
                templateUrl: "tpl/master_prestasi/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/master_prestasi/index.js");
                    }
                  ]
                }
              })
              .state("master.jadwal", {
                url: "/jadwal",
                templateUrl: "tpl/master_jadwal/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/master_jadwal/index.js");
                    }
                  ]
                }
              })
              .state("master.raport", {
                url: "/raport",
                templateUrl: "tpl/master_raport/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/master_raport/index.js");
                    }
                  ]
                }
              })
                .state("master.finger", {
                    url: "/finger",
                    templateUrl: "tpl/master_finger/index.html",
                    resolve: {
                        deps: [
                            "$ocLazyLoad",
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load("tpl/master_finger/index.js");
                            }
                        ]
                    }
                })

              .state("transaksi", {
                url: "/transaksi",
                templateUrl: "tpl/app.html"
              })
              .state("transaksi.pelanggaran", {
                url: "/transaksi-pelanggaran",
                templateUrl: "tpl/transaksi_pelanggaran/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(
                        "tpl/transaksi_pelanggaran/index.js"
                      );
                    }
                  ]
                }
              })
              .state("transaksi.perizinan_siswa", {
                url: "/transaksi-perizinan-siswa",
                templateUrl: "tpl/transaksi_perizinan_siswa/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(
                        "tpl/transaksi_perizinan_siswa/index.js"
                      );
                    }
                  ]
                }
              })
              .state("transaksi.perizinan_guru", {
                url: "/transaksi-perizinan-guru",
                templateUrl: "tpl/transaksi_perizinan_guru/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(
                        "tpl/transaksi_perizinan_guru/index.js"
                      );
                    }
                  ]
                }
              })
              .state("transaksi.finger", {
                url: "/tarik-data-finger",
                templateUrl: "tpl/transaksi_finger/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad
                        .load(["ngFileUpload"])
                        .then(function() {
                          return $ocLazyLoad.load("tpl/transaksi_finger/index.js")
                        });
                    }
                  ]
                }
              })

              .state("transaksi.absensi", {
                url: "/absensi-siswa",
                templateUrl: "tpl/transaksi_absensi/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad
                        .load(["daterangepicker"])
                        .then(function() {
                          return $ocLazyLoad.load(
                            "tpl/transaksi_absensi/index.js"
                          );
                        });
                    }
                  ]
                }
              })
              /** Router laporan */
              .state("laporan", {
                url: "/laporan",
                templateUrl: "tpl/app.html"
              })
              .state("laporan.artikel", {
                url: "/laporan-artikel",
                templateUrl: "tpl/l_artikel/index.html",
                resolve: {
                  deps: [
                    "$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load("tpl/l_artikel/l_artikel.js");
                    }
                  ]
                }
              })
                .state("import", {
                url: "/import",
                templateUrl: "tpl/app.html"
                })
                .state("import.guru", {
                    url: "/import-guru",
                    templateUrl: "tpl/import_guru/index.html",
                    resolve: {
                        deps: [
                            "$ocLazyLoad",
                            function($ocLazyLoad) {
                                return $ocLazyLoad.load("tpl/import_guru/index.js");
                            }
                        ]
                    }
                });
        }
    ]);
